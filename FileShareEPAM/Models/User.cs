﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileShareEPAM.Models
{
    public class User
    {
        public int UserID { get; set; }
        public string Name{ get; set; }
        public string Password{ get; set; }
        public string Email { get; set; }
        
        public virtual List<Roles> Roles { get; set; }
        
        public virtual List<File> Files { get; set; }

        public bool InRoles(string roles)
        {
            if (string.IsNullOrWhiteSpace(roles)) 
            { 
                return false; 
            }

            return roles.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                         .Any(r => Roles.Any(p => r.Equals(p.RoleName, StringComparison.InvariantCultureIgnoreCase)));
        }
    }
}