﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FileShareEPAM.Models
{
    public class File
    {
        [Key]
        public int FileID { get; set; }
        public string Path { get; set; }
        public long Size { get; set; }
        public string Permission { get; set; }
        
        public int UserID{ get; set; }
        public virtual User User { get; set; }
    }
}