﻿using FileShareEPAM.CustomAuthentication;
using FileShareEPAM.Filters;
using FileShareEPAM.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace FileShareEPAM.Controllers
{
    [CustomAuthorizeAttribute]
    public class BaseController : Controller
    {
       

        public PartialViewResult ModalView(string header, string message)
        {
            return PartialView("_ModalPartial", new ModalWindow
            {
                Header = header,
                Message = message
            });
        }

        public ViewResult Error()
        {
            return View("Error");
        }
    }
}
