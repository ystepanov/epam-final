﻿using FileShareEPAM.CustomAuthentication;
using FileShareEPAM.DataAccessLayer.Abstact;
using FileShareEPAM.Filters;
using FileShareEPAM.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FileShareEPAM.Controllers
{
    public class FileController : BaseController
    {
        [Inject]
        public IFileRepository Repo { get; set; }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult DownloadFromShortLink(int id)
        {
            try
            {
                var file = Repo.DownloadtFileByShorLink(id);
                if (file == null)
                    return RedirectToAction("Error");
                return File(file.Path, "application/octet-stream", Path.GetFileName(file.Path));
            }
            catch (Exception)
            {
                return RedirectToAction("Error");
            }
        }

        [HttpGet]
        public PartialViewResult ListOfFiles(string filter = "all")
        {
            var result = Repo
                        .GetFilesInformation(ControllerContext.HttpContext.User.Identity.Name, filter)
                        .ToArray()
                        .Select(f => new FileInformation()
                        {
                            Name = Path.GetFileName(f.Path),
                            Permission = f.Permission,
                            Size = f.Size
                        });

            var resultView = PartialView("_ListOfFilesPartial", result);
            return resultView;
        }

        [HttpGet]
        public ActionResult FileUpload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FileUpload(HttpPostedFileBase file, bool isPrivate)
        {
            try
            {
                if (file == null || file.ContentLength < 1)
                    throw new NullReferenceException("There is no file or file is empty");
                if (file.ContentLength > 50000000)
                    throw new ArgumentException("File is too long");
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/App_Data/uploads/" + ControllerContext.HttpContext.User.Identity.Name), fileName);
                var permission = isPrivate ? "private" : "common";
                Repo.UploadFile(file.InputStream, path, permission, Repo.GetUserID(ControllerContext.HttpContext.User.Identity.Name));
            }
            catch (Exception exp)
            {
                ModelState.AddModelError("File upload error", exp.Message);
                return View();
            }
            return RedirectToAction("Index", "Account");
        }

        [HttpGet]
        public ActionResult FileDownload(string fileName)
        {
            var path = Path.Combine(Server.MapPath("~/App_Data/uploads/" + ControllerContext.HttpContext.User.Identity.Name), fileName);
            if (Repo.GetFile(path) == null)
            {
                return ModalView("Error", "File with this name do not exist");
            }
            return File(path, "application/octet-stream", fileName);
        }

        [HttpPost]
        public ActionResult DeleteFile(string fileName)
        {
            try
            {
                var path = Path.Combine(Server.MapPath("~/App_Data/uploads/" + ControllerContext.HttpContext.User.Identity.Name), fileName);
                var file = Repo.GetFile(path);
                if (file == null)
                    throw new System.IO.FileNotFoundException("File do not exist");
                Repo.DeleteFile(file);
            }
            catch (FileNotFoundException exp)
            {
                return ModalView("Error", exp.Message);
            }
            return ListOfFiles("all");
        }

        [HttpGet]
        public PartialViewResult GetShortLink(string fileName)
        {
            try
            {
                Uri uri = Request.Url;
                var path = Path.Combine(Server.MapPath("~/App_Data/uploads/" + ControllerContext.HttpContext.User.Identity.Name), fileName);
                StringBuilder link = new StringBuilder(uri.Scheme + "://" + uri.Host + (uri.IsDefaultPort ? "" : ":" + uri.Port));
                link.Append("/" + Repo.GetFileShortLink(path));
                return ModalView("Short link for this file", link.ToString());
            }
            catch (ArgumentException exp)
            {
                return ModalView("Error", exp.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            Repo.Dispose();
            base.Dispose(disposing);
        }
    }
}
