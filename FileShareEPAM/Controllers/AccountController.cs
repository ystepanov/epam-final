﻿using FileShareEPAM.CustomAuthentication;
using FileShareEPAM.DataAccessLayer.Abstact;
using FileShareEPAM.Filters;
using FileShareEPAM.Infrastructure;
using FileShareEPAM.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FileShareEPAM.Controllers
{
    public class AccountController : BaseController
    {
        [Inject]
        public IAuthenticationCore Auth { get; set; }

        [Inject]
        public IUserRepository Repo { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpGet]
        public ViewResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model,string ReturnUrl)
        {
            if (ModelState.IsValid) 
            { 
                if (Auth.Authenticate(model.Name, model.Password, HttpContext)) 
                {
                    return RedirectToAction("Index");
                } 
                ModelState["Password"].Errors.Add("User does not exist or password is wrong."); 
            } 
            return View(model);
        }

        public ActionResult Logout()
        {
            Auth.DeAuthenticate(HttpContext);
            return RedirectToAction("Login", "Account");
        }

        [AllowAnonymous]
        [HttpGet]
        public ViewResult Register()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(RegisterModel model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userCatalog = Server.MapPath("~/App_Data/uploads/" + model.Name);
                    Repo.RegesterNewUser(model.Name, model.Password, model.Email, userCatalog);
                    return RedirectToAction("Index");
                }
            }
            catch(ArgumentException exp)
            {
                ModelState.AddModelError("Model error", exp.Message);
            }
            return View();
        }
        
        protected override void Dispose(bool disposing)
        {
            Repo.Dispose();
            base.Dispose(disposing);
        }
    }
}
