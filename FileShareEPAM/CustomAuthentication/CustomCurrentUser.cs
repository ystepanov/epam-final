﻿using FileShareEPAM.DataAccessLayer.Abstact;
using FileShareEPAM.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;

namespace FileShareEPAM.CustomAuthentication
{
    public class CustomCurrentUser : ICurrentUser
    {
        private const string  cookieName = "_AUTH_COOKIE";
        
        public HttpContext HttpContext{ get; set; }

        [Inject]
        public IUserRepository Repo { get; set; }

        private IPrincipal currentUser;
        
        public IPrincipal CurrentUser
        {
            get 
            {
                if (currentUser == null)
                {
                    try
                    {
                        HttpCookie authCookie = HttpContext.Request.Cookies.Get(cookieName);
                        if (authCookie != null && !string.IsNullOrEmpty(authCookie.Value))
                        {
                            var ticket = FormsAuthentication.Decrypt(authCookie.Value);
                            currentUser = new CustomPrincipal(ticket.Name, Repo);
                        }
                        else
                        {
                            currentUser = new CustomPrincipal(null, null);
                        }
                    }
                    catch
                    {
                        currentUser = new CustomPrincipal(null, null); 
                    }
                } 
                return currentUser;
            }
        }

        

    }

    public class CustomPrincipal : IPrincipal
    {
        private CustomIdentity userIdentity;

        public CustomPrincipal(string name, IUserRepository repository)
        {
            userIdentity = new CustomIdentity(); 
            userIdentity.Init(name, repository); 
        }
        public IIdentity Identity
        {
            get { return userIdentity; }
        }

        public bool IsInRole(string role)
        {

            return (userIdentity.User != null)
                && (userIdentity.User.InRoles(role));
        }
    }

    public class CustomIdentity : IIdentity
    {
        public User User { get; set; } 

        public string AuthenticationType
        {
            get { return "Custom"; }
        }

        public bool IsAuthenticated
        {
            get { return User != null; }
        }

        public string Name
        {
            get 
            {
                if (User != null)
                {
                    return User.Name;
                }
                return "Anonym";
            }
        }

        public void Init(string userName, IUserRepository repository)
        { 
            if (!string.IsNullOrEmpty(userName)) 
            { 
                User = repository.GetUserByName(userName); 
            } 
        }
    }


}