﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileShareEPAM.CustomAuthentication
{
    public interface IAuthenticationCore
    {
        bool Authenticate(string userName, string password, HttpContextBase httpContext);
        void DeAuthenticate(HttpContextBase httpContext);
    }
}