﻿using FileShareEPAM.DataAccessLayer.Abstact;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace FileShareEPAM.CustomAuthentication
{
    public class CustomLoginCore : IAuthenticationCore
    {
        private const string cookieName = "_AUTH_COOKIE";

        [Inject]
        public IUserRepository Repo { get; set; }

        public bool Authenticate(string userName, string password, HttpContextBase httpContext)
        {
            var user = Repo.GetUserByName(userName);
            if ((user != null) && (user.Password == password))
            {
                SetCokie(userName, httpContext);
                return true;
            }
            return false;
        }

        public void DeAuthenticate(HttpContextBase httpContext)
        {
            var httpCookie = httpContext.Response.Cookies[cookieName];
            if (httpCookie != null)
            {
                httpCookie.Expires = new DateTime(2008, 3, 9, 16, 5, 7, 123);
            }
        }

        private void SetCokie(string userName, HttpContextBase httpContext, bool isPersistent = false)
        {
            var ticket = new FormsAuthenticationTicket(
                1,
                userName,
                DateTime.Now,
                DateTime.Now.Add(FormsAuthentication.Timeout),
                isPersistent,
                string.Empty,
                FormsAuthentication.FormsCookiePath);
            var encTicket = FormsAuthentication.Encrypt(ticket);
            var AuthCookie = new HttpCookie(cookieName)
            {
                Value = encTicket,
                Expires = DateTime.Now.Add(FormsAuthentication.Timeout)
            };
            httpContext.Response.Cookies.Set(AuthCookie);
        }
    }
}