﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileShareEPAM.ViewModels
{
    public class FileInformation
    {
        public string Name { get; set; }
        public long Size { get; set; }
        public string Permission { get; set; }
    }
}