﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileShareEPAM.ViewModels
{
    public class ModalWindow
    {
        public string Header { get; set; }
        public string Message { get; set; }
    }
}