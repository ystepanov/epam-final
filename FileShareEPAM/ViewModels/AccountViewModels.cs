﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace FileShareEPAM.ViewModels
{
    public class LoginModel
    {
        [Required]
        [StringLength(20, MinimumLength = 4)]
        [Display(Name = "User Name")]
        public string Name { get; set; }
        
        [Required]
        [StringLength(20, MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }

    public class RegisterModel
    {
        [Required]
        [StringLength(20, MinimumLength = 4)]
        [Display(Name = "User Name")]
        public string Name { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 4)]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(20, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        
        [Required]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }
    }


}