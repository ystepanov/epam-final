﻿using FileShareEPAM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace FileShareEPAM.DataAccessLayer.Abstact
{
    public interface IFileRepository : IDisposable
    {
        void UploadFile(System.IO.Stream file, string path, string permission, int userID);

        IQueryable<File> GetFilesInformation(string username, string filter);

        File GetFile(string path);

        void DeleteFile(File file);

        string GetFileShortLink(string path);

        File DownloadtFileByShorLink(int fileID);

        int GetUserID(string username);
    }
}
