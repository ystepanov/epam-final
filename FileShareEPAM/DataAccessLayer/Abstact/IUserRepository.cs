﻿using FileShareEPAM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileShareEPAM.DataAccessLayer.Abstact
{
    public interface IUserRepository: IDisposable
    {
        void RegesterNewUser(string username, string password, string email, string userCatalog);
        
        User GetUserByName(string userName);
    }
}