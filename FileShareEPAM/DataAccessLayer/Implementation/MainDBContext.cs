﻿using FileShareEPAM.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FileShareEPAM.DataAccessLayer.Implementation
{
    public class MainDBContext : DbContext
    {
        public DbSet<User> Users{ get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<Roles> Roles{ get; set; }
    }
}