﻿using FileShareEPAM.DataAccessLayer.Abstact;
using FileShareEPAM.Infrastructure;
using FileShareEPAM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace FileShareEPAM.DataAccessLayer.Implementation
{
    public class FileRepository : IFileRepository
    {
        private readonly MainDBContext db;

        public FileRepository()
        {
            db = new MainDBContext();
        }

        #region IFileRepository members

        public void UploadFile(System.IO.Stream file, string path, string permission, int userID)
        {
            if (GetFile(path) != null)
                throw new FileAlreadyExistExeption("File with this name already exist");
            var fileModel = new File()
            {
                Path = path,
                Size = file.Length,
                Permission = permission,
                UserID = userID
            };
            db.Files.Add(fileModel);
            db.SaveChanges();
            using (var fileStream = System.IO.File.Create(path))
            {
                file.CopyTo(fileStream);
            }
        }

        public IQueryable<File> GetFilesInformation(string username, string filter)
        {
            var query = db.Files.AsQueryable<File>().Where(f => f.User.Name == username);
            if (filter != "all")
                query = query.Where(f => f.Permission == filter && f.User.Name == username);
            return query;
        }


        public File GetFile(string path)
        {
            return db.Files.FirstOrDefault(f => f.Path == path);
        }

        public void DeleteFile(File file)
        {
            db.Files.Remove(file);
            db.SaveChanges();
            System.IO.File.Delete(file.Path);
        }

        public string GetFileShortLink(string path)
        {
            var file = GetFile(path);
            if (file == null)
                throw new ArgumentException("File do not exist");
            if (file.Permission == "private")
                throw new ArgumentException("Can`t get short link for private file");
            return file.FileID.ToString();
        }

        public File DownloadtFileByShorLink(int fileID)
        {
            var query = db.Files.Where(x => x.Permission == "common")
                            .FirstOrDefault(x => x.FileID == fileID);
            return query;
        }

        #endregion

        public void Dispose()
        {
            db.Dispose();
        }

        public int GetUserID(string username)
        {
            return db.Users.FirstOrDefault(x => x.Name == username).UserID;
        }
    }
}