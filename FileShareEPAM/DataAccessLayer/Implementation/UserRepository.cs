﻿using FileShareEPAM.DataAccessLayer.Abstact;
using FileShareEPAM.Infrastructure;
using FileShareEPAM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Web;

namespace FileShareEPAM.DataAccessLayer.Implementation
{
    public class UserRepository : IUserRepository
    {
        private readonly MainDBContext db;

        public UserRepository()
        {
            db = new MainDBContext();
        }

        #region IUserRepository members

        public User GetUserByName(string username)
        {
            return db.Users.FirstOrDefault(x => x.Name == username);
        }

        public void RegesterNewUser(string username, string password, string email, string userCatalog)
        {
            if (GetUserByName(username) != null)
                throw new ArgumentException("User with this name already exist");
            if (db.Users.FirstOrDefault(x => x.Email == email) != null)
                throw new ArgumentException("User with this email already exist");
            var user = db.Users.Create();
            user.Name = username;
            user.Password = password;
            user.Email = email;
            db.Users.Add(user);
            db.SaveChanges();
            System.IO.Directory.CreateDirectory(userCatalog);
        }

        #endregion

        public void Dispose()
        {
            db.Dispose();
        }
        
    }
}