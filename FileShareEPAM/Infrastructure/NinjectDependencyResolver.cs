﻿using FileShareEPAM.CustomAuthentication;
using FileShareEPAM.DataAccessLayer.Abstact;
using FileShareEPAM.DataAccessLayer.Implementation;
using Ninject.Web.Common;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FileShareEPAM.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;
        public NinjectDependencyResolver()
        {
            kernel = new StandardKernel();
            AddBindings();
        }
        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }
        private void AddBindings()
        {
            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<IFileRepository>().To<FileRepository>();
            kernel.Bind<ICurrentUser>().To<CustomCurrentUser>().InRequestScope();
            kernel.Bind<IAuthenticationCore>().To<CustomLoginCore>().InRequestScope();
        }

    }
}