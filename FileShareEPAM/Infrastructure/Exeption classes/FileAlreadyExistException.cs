﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileShareEPAM.Infrastructure
{
    public class FileAlreadyExistExeption : Exception
    {
        public FileAlreadyExistExeption(string message)
            : base(message)
        { }

        public FileAlreadyExistExeption()
            : base()
        { }
    }
}