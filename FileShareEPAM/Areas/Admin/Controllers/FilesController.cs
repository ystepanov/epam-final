﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FileShareEPAM.Models;
using FileShareEPAM.DataAccessLayer.Implementation;

namespace FileShareEPAM.Areas.Admin.Controllers
{
    public class FilesController : Controller
    {
        private MainDBContext db = new MainDBContext();

        //
        // GET: /Admin/File/

        public ActionResult Index()
        {
            var files = db.Files.Include(f => f.User);
            return View(files.ToList());
        }

        //
        // GET: /Admin/File/Details/5

        public ActionResult Details(int id = 0)
        {
            File file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

        //
        // GET: /Admin/File/Create

        public ActionResult Create()
        {
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name");
            return View();
        }

        //
        // POST: /Admin/File/Create

        [HttpPost]
        public ActionResult Create(File file)
        {
            if (ModelState.IsValid)
            {
                db.Files.Add(file);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name", file.UserID);
            return View(file);
        }

        //
        // GET: /Admin/File/Edit/5

        public ActionResult Edit(int id = 0)
        {
            File file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name", file.UserID);
            return View(file);
        }

        //
        // POST: /Admin/File/Edit/5

        [HttpPost]
        public ActionResult Edit(File file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(file).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserID = new SelectList(db.Users, "UserID", "Name", file.UserID);
            return View(file);
        }

        //
        // GET: /Admin/File/Delete/5

        public ActionResult Delete(int id = 0)
        {
            File file = db.Files.Find(id);
            if (file == null)
            {
                return HttpNotFound();
            }
            return View(file);
        }

        //
        // POST: /Admin/File/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            File file = db.Files.Find(id);
            db.Files.Remove(file);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}